import gulp from 'gulp';
import transform from 'gulp-transform';
import _ from 'lodash';
import merge from 'merge-stream';
import rollup from 'rollup-stream';
import source from 'vinyl-source-stream';
import rollupConfig from './rollup.config';

const packageFields = `name version description keywords homepage bugs license \
author contributors bin man directories repository dependencies \
peerDependencies bundledDependencies bundleDependencies optionalDependencies \
engines os cpu`.split(' ');

const binRollupConfig = Object.assign({}, rollupConfig, {
  input: 'src/bin.js',
  banner: '#!/usr/bin/env node',
});

gulp.task('dist', [], () =>
  merge(
    rollup(rollupConfig).pipe(source('index.js')),
    rollup(binRollupConfig).pipe(source('bin.js')),
    gulp.src('package.json').pipe(transform('utf8', contents =>
      JSON.stringify(_.pick(JSON.parse(contents), packageFields), null, 2))),
    gulp.src(['README.md', 'LICENSE.txt']),
  ).pipe(gulp.dest('dist')));
