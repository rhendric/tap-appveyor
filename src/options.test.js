import test from 'ava';
import { defaults, resolveOptions } from './options';

test.serial("read apiUrl from env", t => {
  process.env.APPVEYOR_API_URL = 'foo';
  t.is(resolveOptions().apiUrl, 'foo');
  t.is(resolveOptions({}).apiUrl, 'foo');
  t.is(resolveOptions({ apiUrl: 'bar' }).apiUrl, 'bar');
});

test.serial("read batchSize from env", t => {
  process.env.APPVEYOR_BATCH_SIZE = '30';
  t.is(resolveOptions().batchSize, 30);
  t.is(resolveOptions({}).batchSize, 30);
  t.is(resolveOptions({ batchSize: 50 }).batchSize, 50);
});

test.serial("read batchIntervalInMs from env", t => {
  process.env.APPVEYOR_BATCH_INTERVAL_IN_MS = '200';
  t.is(resolveOptions().batchIntervalInMs, 200);
  t.is(resolveOptions({}).batchIntervalInMs, 200);
  t.is(resolveOptions({ batchIntervalInMs: 50 }).batchIntervalInMs, 50);
});

test.serial("read from defaults", t => {
  delete process.env.APPVEYOR_API_URL;
  delete process.env.APPVEYOR_BATCH_SIZE;
  delete process.env.APPVEYOR_BATCH_INTERVAL_IN_MS;
  t.is(resolveOptions().apiUrl, defaults.apiUrl);
  t.is(resolveOptions().batchSize, defaults.batchSize);
  t.is(resolveOptions().batchIntervalInMs, defaults.batchIntervalInMs);
  t.is(resolveOptions({ batchSize: 10 }).apiUrl, defaults.apiUrl);
  t.is(resolveOptions({ apiUrl: 'foo' }).batchSize, defaults.batchSize);
  t.is(resolveOptions({}).batchIntervalInMs, defaults.batchIntervalInMs);
});
