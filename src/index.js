import { runInBatches } from './runInBatches';
import { defaults, resolveOptions } from './options';

const request = require('request');
const TapParser = require('tap-parser');
const url = require('url');

const appveyor = options => {
  const { apiUrl, batchSize, batchIntervalInMs } = resolveOptions(options);
  const parser = new TapParser();
  const postBatch = request.defaults({
    url: url.resolve(apiUrl, 'api/tests/batch'),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  });
  const batcher = runInBatches(
    (events, cb) => postBatch({ body: JSON.stringify(events) },
      (e, r, b) => e ? cb(e) : r.statusCode < 300 ? cb() : cb(new Error(b))),
    batchSize,
    batchIntervalInMs,
    err => err ? parser.emit('error', err) : parser.emit('report-complete'));
  const nameCounts = new Map();
  const uniquify = name => {
    if (nameCounts.has(name)) {
      const count = nameCounts.get(name) + 1;
      nameCounts.set(name, count);
      return `${name} (${count})`;
    } else {
      nameCounts.set(name, 1);
      return name;
    }
  };
  parser.on('assert', event => batcher.push({
    testName: uniquify(event.name),
    outcome: event.ok ? 'Passed'
      : event.skip ? 'Skipped'
      : event.todo ? 'Ignored'
      : 'Failed',
    ErrorMessage: event.diag ? event.diag.message : void 0,
    ErrorStackTrace: event.diag ? event.diag.stack : void 0,
  }));
  parser.on('complete', batcher.end);
  return parser;
};
appveyor.defaults = defaults;

export default appveyor;
