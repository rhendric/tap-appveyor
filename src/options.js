export const defaults = {
  batchIntervalInMs: 1000,
  batchSize: 100,
};

export const resolveOptions = options => {
  const { env } = process;
  options || (options = {});
  return {
    apiUrl: options.apiUrl
      || env.APPVEYOR_API_URL,
    batchIntervalInMs: options.batchIntervalInMs
      || +env.APPVEYOR_BATCH_INTERVAL_IN_MS
      || defaults.batchIntervalInMs,
    batchSize: options.batchSize
      || +env.APPVEYOR_BATCH_SIZE
      || defaults.batchSize,
  };
};
