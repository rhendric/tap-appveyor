import EventEmitter from 'events';
import http from 'http';
import path from 'path';
import test from 'ava';
import { json } from 'body-parser';
import escapeRE from 'escape-string-regexp';
import express from 'express';
import lolex from 'lolex';
import { Test } from 'tap';
import appveyor from '.';

const trueSetImmediate = setImmediate;
const clock = lolex.install();
const clockEvents = new EventEmitter();
for (const method in clock) {
  if (clock.hasOwnProperty(method) && method in global) {
    const orig = clock[method];
    clock[method] = (...args) => {
      const result = orig.apply(clock, args);
      trueSetImmediate(() => clockEvents.emit(method, result));
      return result;
    };
  }
}

test.afterEach("no outstanding clock timers", t =>
  t.deepEqual(clock.timers, {}));

function makeTestAndReporter(options) {
  const tap = new Test();
  const reporter = appveyor(options);
  const reporterPromise = eventPromise(reporter, 'report-complete', 'error');
  tap.pipe(reporter);
  return [tap, reporterPromise];
}

const callbackPromise = (source, name, ...args) => new Promise((res, rej) =>
  source[name](...args, err => err ? rej(err) : res()));

const eventPromise = (source, name, errorName) => new Promise((res, rej) => {
  source.once(name, res);
  if (errorName) {
    source.once(errorName, rej);
  }
});

const batchSize = 3;
const { batchIntervalInMs } = appveyor.defaults;

function fixture(t, onStart, onEnd = () => {}) {
  const reports = [];
  let testIterator;
  const app = express()
    .use(json())
    .post('/api/tests/batch', (req, res) => {
      try {
        reports.push(...req.body);
        if (testIterator && testIterator.next().done) {
          testIterator = void 0;
        }
        res.status(200).send('');
      } catch (e) {
        res.status(500).send(e.message);
      }
    });
  const server = http.createServer(app).listen();
  const testPromise = (async () => {
    try {
      await eventPromise(server, 'listening');
      const { address, port } = server.address();
      const apiUrl = `http://${address}:${port}`;
      const [tap, reporter] = makeTestAndReporter({ apiUrl, batchSize });
      testIterator = onStart(t, tap, reports);
      if (testIterator && testIterator.next().done) {
        testIterator = void 0;
      }
      return await reporter;
    } finally {
      await callbackPromise(server, 'close');
    }
  })();
  return typeof onEnd === 'string'
    ? t.throws(testPromise, onEnd)
    : testPromise.then(() => onEnd(t, reports));
}

const failFile = path.relative('.', __filename);
const failLine = __line + 5;

test.serial("all basic test result types", fixture,
  (t, tap) => {
    tap.pass('one');
    tap.fail('two'); // `failLine` should point here
    tap.fail('three', { todo: true });
    tap.fail('four', { skip: true });
    tap.end();
  }, (t, reports) => {
    t.expect(reports, 'to exhaustively satisfy', [
      {
        testName: 'one',
        outcome: 'Passed',
      },
      {
        testName: 'two',
        outcome: 'Failed',
        ErrorStackTrace: new RegExp(
          `^[^ ]* \\(${escapeRE(failFile)}:${failLine}:9\\)\\n`),
      },
      {
        testName: 'three',
        outcome: 'Ignored',
      },
      {
        testName: 'four',
        outcome: 'Skipped',
      }
    ]);
  });

test.serial("uniquify duplicate test names", fixture,
  (t, tap) => {
    for (let i = 0; i < 5; i++) {
      tap.pass('foo');
    }
    tap.end();
  }, (t, reports) => {
    const expected = ['foo', 'foo (2)', 'foo (3)', 'foo (4)', 'foo (5)'];
    t.deepEqual(reports.map(r => r.testName), expected);
  });

test.serial("limit batch to batchSize tests", fixture,
  function* (t, tap, reports) {
    for (let i = 0; i < 10; i++) {
      tap.pass('foo');
    }
    tap.end();
    yield;
    t.is(reports.length, batchSize);
    yield;
    t.is(reports.length, 2 * batchSize);
  }, (t, reports) => {
    t.is(reports.length, 10);
  });

test.serial("send once there's a full batch", fixture,
  function* (t, tap, reports) {
    for (let i = 0; i < 5; i++) {
      tap.pass('foo');
    }
    yield;
    t.is(reports.length, batchSize);
    tap.end();
  }, (t, reports) => {
    t.is(reports.length, 5);
  });

test.serial("send once batchInterval has elapsed", fixture,
  function* (t, tap, reports) {
    tap.pass('foo');
    // TAP parsing is always one test behind, so this next test ensures the
    // previous will be in the batch
    tap.pass('foo');
    clock.tick(batchIntervalInMs);
    yield;
    t.is(reports.length, 1);
    tap.end();
  }, (t, reports) => {
    t.is(reports.length, 2);
  });

test.serial("send once batchInterval has elapsed, after initial batch", fixture,
  function* (t, tap, reports) {
    for (let i = 0; i < 5; i++) {
      tap.pass('foo');
    }
    yield;
    t.is(reports.length, batchSize);
    clockEvents.once('setInterval', () => clock.tick(batchIntervalInMs));
    yield;
    t.is(reports.length, 4); // Again, we're one test behind, until end is sent.
    tap.end();
  }, (t, reports) => {
    t.is(reports.length, 5);
  });

test.serial("gracefully handle server errors", fixture,
  function* (t, tap) {
    for (let i = 0; i < 5; i++) {
      tap.pass('foo');
    }
    yield;
    tap.end();
    throw new Error('server error');
  }, 'server error');

test.serial("gracefully handle connection errors", t => {
  const [tap, reporter] = makeTestAndReporter({ apiUrl: 'fake:url' });
  tap.pass('foo');
  tap.end();
  return t.throws(reporter, 'Invalid protocol: fake:');
});
