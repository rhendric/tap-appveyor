export function runInBatches(action, batchSize, batchIntervalInMs, cb) {
  const queue = [];
  let complete = false;
  const flush = () => {
    const items = queue.splice(0, batchSize);
    if (!items.length) {
      return complete && cb();
    }
    if (waiting) {
      clearInterval(waiting);
      waiting = null;
    }
    action(items, err => {
      if (err) {
        return cb(err);
      }
      if (complete || queue.length >= batchSize) {
        flush();
      } else {
        waiting = setInterval(flush, batchIntervalInMs);
      }
    });
  };
  let waiting = setInterval(flush, batchIntervalInMs);

  return {
    push(item) {
      queue.push(item);
      if (waiting && queue.length >= batchSize) {
        flush();
      }
    },

    end() {
      complete = true;
      if (waiting) {
        clearInterval(waiting);
        waiting = null;
        flush();
      }
    },
  };
}
