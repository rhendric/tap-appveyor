# tap-appveyor

[![GitLab CI pipeline status](https://gitlab.com/rhendric/tap-appveyor/badges/master/pipeline.svg)](https://gitlab.com/rhendric/tap-appveyor/commits/master) [![Codecov status](https://img.shields.io/codecov/c/gl/rhendric/tap-appveyor.svg)](https://codecov.io/gl/rhendric/tap-appveyor)

tap-appveyor is a small utility for sending TAP output from your test framework to AppVeyor's test console.

## Usage

### Streaming

```javascript
const test = require('tape');
const appveyor = require('tap-appveyor');

test.createStream().pipe(appveyor());
```

### CLI

```
ava --tap | tap-appveyor
```

## Options

The below options can be provided as environment variables, CLI arguments, or as entries in an options object passed to the module function. The latter two take precedence over environment variables.

|Option Name      |CLI Argument          |Environment Variable             |Default                                                  |
|-----------------|----------------------|---------------------------------|---------------------------------------------------------|
|apiUrl           |--api-url             |APPVEYOR\_API\_URL               |unset, but AppVeyor sets the environment variable for you|
|batchIntervalInMs|--batch-interval-in-ms|APPVEYOR\_BATCH\_INTERVAL\_IN\_MS|1000                                                     |
|batchSize        |--batch-size          |APPVEYOR\_BATCH\_SIZE            |100                                                      |

## Credits

Heavily inspired by, if not a straight ripoff of, [mocha-appveyor-reporter](https://github.com/tolbertam/mocha-appveyor-reporter).
