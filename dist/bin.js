#!/usr/bin/env node
'use strict';

const yargs = require('yargs');
const appveyor = require('.');
const opts = yargs.option('api-url', {
  describe: "AppVeyor API endpoint (required if not running in an AppVeyor job)"
}).option('batch-interval-in-ms', {
  describe: "Maximum amount of time between updates to AppVeyor",
  defaultDescription: appveyor.defaults.batchIntervalInMs
}).option('batch-size', {
  describe: "Maximum number of test results between updates to AppVeyor",
  defaultDescription: appveyor.defaults.batchSize
}).strict().argv;
process.stdin.pipe(appveyor(opts));
process.stdin.pipe(process.stdout);
