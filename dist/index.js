'use strict';

function runInBatches(action, batchSize, batchIntervalInMs, cb) {
  const queue = [];
  let complete = false;
  const flush = () => {
    const items = queue.splice(0, batchSize);
    if (!items.length) {
      return complete && cb();
    }
    if (waiting) {
      clearInterval(waiting);
      waiting = null;
    }
    action(items, err => {
      if (err) {
        return cb(err);
      }
      if (complete || queue.length >= batchSize) {
        flush();
      } else {
        waiting = setInterval(flush, batchIntervalInMs);
      }
    });
  };
  let waiting = setInterval(flush, batchIntervalInMs);
  return {
    push(item) {
      queue.push(item);
      if (waiting && queue.length >= batchSize) {
        flush();
      }
    },
    end() {
      complete = true;
      if (waiting) {
        clearInterval(waiting);
        waiting = null;
        flush();
      }
    }
  };
}

const defaults = {
  batchIntervalInMs: 1000,
  batchSize: 100
};
const resolveOptions = options => {
  var _process = process;
  const env = _process.env;
  options || (options = {});
  return {
    apiUrl: options.apiUrl || env.APPVEYOR_API_URL,
    batchIntervalInMs: options.batchIntervalInMs || +env.APPVEYOR_BATCH_INTERVAL_IN_MS || defaults.batchIntervalInMs,
    batchSize: options.batchSize || +env.APPVEYOR_BATCH_SIZE || defaults.batchSize
  };
};

const request = require('request');
const TapParser = require('tap-parser');
const url = require('url');
const appveyor = options => {
  var _resolveOptions = resolveOptions(options);
  const apiUrl = _resolveOptions.apiUrl,
        batchSize = _resolveOptions.batchSize,
        batchIntervalInMs = _resolveOptions.batchIntervalInMs;
  const parser = new TapParser();
  const postBatch = request.defaults({
    url: url.resolve(apiUrl, 'api/tests/batch'),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    }
  });
  const batcher = runInBatches((events, cb) => postBatch({ body: JSON.stringify(events) }, (e, r, b) => e ? cb(e) : r.statusCode < 300 ? cb() : cb(new Error(b))), batchSize, batchIntervalInMs, err => err ? parser.emit('error', err) : parser.emit('report-complete'));
  const nameCounts = new Map();
  const uniquify = name => {
    if (nameCounts.has(name)) {
      const count = nameCounts.get(name) + 1;
      nameCounts.set(name, count);
      return `${name} (${count})`;
    } else {
      nameCounts.set(name, 1);
      return name;
    }
  };
  parser.on('assert', event => batcher.push({
    testName: uniquify(event.name),
    outcome: event.ok ? 'Passed' : event.skip ? 'Skipped' : event.todo ? 'Ignored' : 'Failed',
    ErrorMessage: event.diag ? event.diag.message : void 0,
    ErrorStackTrace: event.diag ? event.diag.stack : void 0
  }));
  parser.on('complete', batcher.end);
  return parser;
};
appveyor.defaults = defaults;

module.exports = appveyor;
