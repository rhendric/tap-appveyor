import relative from 'require-relative';

const { wrapCallSite } = relative('source-map-support',
  relative.resolve('ava'));

Object.defineProperty(global, '__line', {
  get() {
    const pst = Error.prepareStackTrace;
    Error.prepareStackTrace = (e, sites) =>
      wrapCallSite(sites[1]).getLineNumber();
    try {
      return new Error().stack;
    } finally {
      Error.prepareStackTrace = pst;
    }
  }
});
